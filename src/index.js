import countryCodes from './codes/country';
import cityCodesNL from './codes/netherlands';

// These numbers will always be valid.
const SPECIAL_NUMBERS = ['112', '911'];

export default {
    defaultCountry: null,
    /**
     * Format a number by the following rules:
     * 1. Remove all characters except digits, '#', '+' and '*'.
     * 2. If the number starts with a single zero, it is a national number.
     *    Replace the first digit with the country code of the user.
     * 3. If the number starts with 14, prepend country code (14 + municipality area code).
     * 4. If the number starts with two zero's, replace the first two digits with a '+'.
     *
     * @return {String} Formatted international phonenumber.
     */
    formatNumber(number, countryCode) {
        countryCode = countryCode || this.defaultCountry;

        let normalized = String(number).replace(/[^#*0-9\+]/g, '');
        const country = this.getCountryFromCode(countryCode);

        // Convert national number to international.
        if (/^0[^0]/.test(normalized)) {
            normalized = country.dial_code + normalized.substr(1);
        }

        // Convert beginning '00' to '+'.
        if (/^00/.test(normalized)) {
            normalized = `+${normalized.substr(2)}`;
        }

        if (/^14/.test(normalized)) {
            normalized = country.dial_code + normalized;
        }

        return normalized;
    },
    /**
     * Detect the country of a formatted phonenumber.
     *
     * @return {Object} Object of country if a corresponding country if found,
     *         Otherwise null.
     */
    getCountryFromNumber(number) {
        const isInternal = number.charAt(0) === '*';

        if (isInternal) {
            return this.getInternal();
        }

        if (number.length > 1) {
            return countryCodes.find((country) =>
                number.indexOf(country.dial_code) > -1 && country
            );
        }

        return null;
    },
    /**
     * Detect the city of a formatted phonenumber.
     *
     * @return {Object} Object of city if a corresponding city if found,
     *         Otherwise null.
     */
    getCityFromNumber(number) {
        // We only have data from Dutch cities at the moment.
        let chosenCity = null;

        if (number.length > 3) {
            chosenCity = cityCodesNL.find((city) =>
                number.indexOf(city.dial_code) > -1 && city
            );
        }

        return chosenCity;
    },
    /**
     * Formats the number and looks up the country and city of it.
     *
     * @return {Object} Always returns 'number' (formatted),
     *         and 'country' (long name) and/or 'city' (long name) if found.
     */
    getFormatted(number, countryCode) {
        const output = {};
        const formattedNumber = this.formatNumber(number, countryCode);
        const country = this.getCountryFromNumber(formattedNumber);
        const city = this.getCityFromNumber(formattedNumber);

        output.number = formattedNumber;

        if (country) {
            output.country = country.country;
        }

        if (country && city) {
            output.city = city.city;
        }

        return output;
    },
    /**
     * Looks up full country info (long name, dial code and short code)
     * of a given short country code (ex: 'nl').
     *
     * @return {Object} Object of country if a corresponding country if found,
     *         Otherwise null.
     */
    getCountryFromCode(code) {
        code = code.toUpperCase();
        return countryCodes.find((country) =>
            code === country.code && country
        );
    },
    /**
     * Get format of internal number.
     *
     * @return {Object}
     */
    getInternal() {
        return {
            country: 'Internal number',
            dial_code: '*',
            code: 'internal',
        };
    },
    /**
     * @return {boolean} Whether |value| is a (dialable) phone number.
     */
    isValidNumber(number) {
        number = String(number);

        // TODO(robwu): Make this pattern a bit stricter.
        if (/^\s*\+?[#*0-9\-\+ ]+$/.test(number)) {
            if (SPECIAL_NUMBERS.indexOf(number) === -1 && number.replace(/\D/g, '').length < 3) {
                // Must have at least four numbers and not be an alarm number
                return false;
            }
            return true;
        }
        return false;
    },
};
