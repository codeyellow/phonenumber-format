const rollup = require('rollup');
const babel = require('rollup-plugin-babel');

rollup.rollup({
    entry: './src/index.js',
    plugins: [
        babel({
            exclude: 'node_modules/**',
        }),
    ],
}).then((bundle) => {
    bundle.write({
        format: 'umd',
        moduleId: 'phonenumber-format',
        moduleName: 'phonenumberFormat',
        dest: 'dist/phonenumber-format.js',
    });
}).catch((err) => {
    console.log(String(err));
    process.exit(1);
});
