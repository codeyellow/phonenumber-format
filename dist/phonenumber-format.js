(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define('phonenumber-format', factory) :
    (global.phonenumberFormat = factory());
}(this, function () { 'use strict';

    var countryCodes = [{
        country: 'Israel',
        dial_code: '+972',
        code: 'IL'
    }, {
        country: 'Afghanistan',
        dial_code: '+93',
        code: 'AF'
    }, {
        country: 'Albania',
        dial_code: '+355',
        code: 'AL'
    }, {
        country: 'Algeria',
        dial_code: '+213',
        code: 'DZ'
    }, {
        country: 'AmericanSamoa',
        dial_code: '+1684',
        code: 'AS'
    }, {
        country: 'Andorra',
        dial_code: '+376',
        code: 'AD'
    }, {
        country: 'Angola',
        dial_code: '+244',
        code: 'AO'
    }, {
        country: 'Anguilla',
        dial_code: '+1264',
        code: 'AI'
    }, {
        country: 'Antigua and Barbuda',
        dial_code: '+1268',
        code: 'AG'
    }, {
        country: 'Argentina',
        dial_code: '+54',
        code: 'AR'
    }, {
        country: 'Armenia',
        dial_code: '+374',
        code: 'AM'
    }, {
        country: 'Aruba',
        dial_code: '+297',
        code: 'AW'
    }, {
        country: 'Australia',
        dial_code: '+61',
        code: 'AU'
    }, {
        country: 'Austria',
        dial_code: '+43',
        code: 'AT'
    }, {
        country: 'Azerbaijan',
        dial_code: '+994',
        code: 'AZ'
    }, {
        country: 'Bahamas',
        dial_code: '+1242',
        code: 'BS'
    }, {
        country: 'Bahrain',
        dial_code: '+973',
        code: 'BH'
    }, {
        country: 'Bangladesh',
        dial_code: '+880',
        code: 'BD'
    }, {
        country: 'Barbados',
        dial_code: '+1246',
        code: 'BB'
    }, {
        country: 'Belarus',
        dial_code: '+375',
        code: 'BY'
    }, {
        country: 'Belgium',
        dial_code: '+32',
        code: 'BE'
    }, {
        country: 'Belize',
        dial_code: '+501',
        code: 'BZ'
    }, {
        country: 'Benin',
        dial_code: '+229',
        code: 'BJ'
    }, {
        country: 'Bermuda',
        dial_code: '+1441',
        code: 'BM'
    }, {
        country: 'Bhutan',
        dial_code: '+975',
        code: 'BT'
    }, {
        country: 'Bosnia and Herzegovina',
        dial_code: '+387',
        code: 'BA'
    }, {
        country: 'Botswana',
        dial_code: '+267',
        code: 'BW'
    }, {
        country: 'Brazil',
        dial_code: '+55',
        code: 'BR'
    }, {
        country: 'British Indian Ocean Territory',
        dial_code: '+246',
        code: 'IO'
    }, {
        country: 'Bulgaria',
        dial_code: '+359',
        code: 'BG'
    }, {
        country: 'Burkina Faso',
        dial_code: '+226',
        code: 'BF'
    }, {
        country: 'Burundi',
        dial_code: '+257',
        code: 'BI'
    }, {
        country: 'Cambodia',
        dial_code: '+855',
        code: 'KH'
    }, {
        country: 'Cameroon',
        dial_code: '+237',
        code: 'CM'
    }, {
        country: 'Canada',
        dial_code: '+1',
        code: 'CA'
    }, {
        country: 'Cape Verde',
        dial_code: '+238',
        code: 'CV'
    }, {
        country: 'Cayman Islands',
        dial_code: '+345',
        code: 'KY'
    }, {
        country: 'Central African Republic',
        dial_code: '+236',
        code: 'CF'
    }, {
        country: 'Chad',
        dial_code: '+235',
        code: 'TD'
    }, {
        country: 'Chile',
        dial_code: '+56',
        code: 'CL'
    }, {
        country: 'China',
        dial_code: '+86',
        code: 'CN'
    }, {
        country: 'Christmas Island',
        dial_code: '+61',
        code: 'CX'
    }, {
        country: 'Colombia',
        dial_code: '+57',
        code: 'CO'
    }, {
        country: 'Comoros',
        dial_code: '+269',
        code: 'KM'
    }, {
        country: 'Congo',
        dial_code: '+242',
        code: 'CG'
    }, {
        country: 'Cook Islands',
        dial_code: '+682',
        code: 'CK'
    }, {
        country: 'Costa Rica',
        dial_code: '+506',
        code: 'CR'
    }, {
        country: 'Croatia',
        dial_code: '+385',
        code: 'HR'
    }, {
        country: 'Cuba',
        dial_code: '+53',
        code: 'CU'
    }, {
        country: 'Cyprus',
        dial_code: '+537',
        code: 'CY'
    }, {
        country: 'Czech Republic',
        dial_code: '+420',
        code: 'CZ'
    }, {
        country: 'Denmark',
        dial_code: '+45',
        code: 'DK'
    }, {
        country: 'Djibouti',
        dial_code: '+253',
        code: 'DJ'
    }, {
        country: 'Dominica',
        dial_code: '+1767',
        code: 'DM'
    }, {
        country: 'Dominican Republic',
        dial_code: '+1849',
        code: 'DO'
    }, {
        country: 'Ecuador',
        dial_code: '+593',
        code: 'EC'
    }, {
        country: 'Egypt',
        dial_code: '+20',
        code: 'EG'
    }, {
        country: 'El Salvador',
        dial_code: '+503',
        code: 'SV'
    }, {
        country: 'Equatorial Guinea',
        dial_code: '+240',
        code: 'GQ'
    }, {
        country: 'Eritrea',
        dial_code: '+291',
        code: 'ER'
    }, {
        country: 'Estonia',
        dial_code: '+372',
        code: 'EE'
    }, {
        country: 'Ethiopia',
        dial_code: '+251',
        code: 'ET'
    }, {
        country: 'Faroe Islands',
        dial_code: '+298',
        code: 'FO'
    }, {
        country: 'Fiji',
        dial_code: '+679',
        code: 'FJ'
    }, {
        country: 'Finland',
        dial_code: '+358',
        code: 'FI'
    }, {
        country: 'France',
        dial_code: '+33',
        code: 'FR'
    }, {
        country: 'French Guiana',
        dial_code: '+594',
        code: 'GF'
    }, {
        country: 'French Polynesia',
        dial_code: '+689',
        code: 'PF'
    }, {
        country: 'Gabon',
        dial_code: '+241',
        code: 'GA'
    }, {
        country: 'Gambia',
        dial_code: '+220',
        code: 'GM'
    }, {
        country: 'Georgia',
        dial_code: '+995',
        code: 'GE'
    }, {
        country: 'Germany',
        dial_code: '+49',
        code: 'DE'
    }, {
        country: 'Ghana',
        dial_code: '+233',
        code: 'GH'
    }, {
        country: 'Gibraltar',
        dial_code: '+350',
        code: 'GI'
    }, {
        country: 'Greece',
        dial_code: '+30',
        code: 'GR'
    }, {
        country: 'Greenland',
        dial_code: '+299',
        code: 'GL'
    }, {
        country: 'Grenada',
        dial_code: '+1473',
        code: 'GD'
    }, {
        country: 'Guadeloupe',
        dial_code: '+590',
        code: 'GP'
    }, {
        country: 'Guam',
        dial_code: '+1671',
        code: 'GU'
    }, {
        country: 'Guatemala',
        dial_code: '+502',
        code: 'GT'
    }, {
        country: 'Guinea',
        dial_code: '+224',
        code: 'GN'
    }, {
        country: 'Guinea-Bissau',
        dial_code: '+245',
        code: 'GW'
    }, {
        country: 'Guyana',
        dial_code: '+595',
        code: 'GY'
    }, {
        country: 'Haiti',
        dial_code: '+509',
        code: 'HT'
    }, {
        country: 'Honduras',
        dial_code: '+504',
        code: 'HN'
    }, {
        country: 'Hungary',
        dial_code: '+36',
        code: 'HU'
    }, {
        country: 'Iceland',
        dial_code: '+354',
        code: 'IS'
    }, {
        country: 'India',
        dial_code: '+91',
        code: 'IN'
    }, {
        country: 'Indonesia',
        dial_code: '+62',
        code: 'ID'
    }, {
        country: 'Iraq',
        dial_code: '+964',
        code: 'IQ'
    }, {
        country: 'Ireland',
        dial_code: '+353',
        code: 'IE'
    }, {
        country: 'Israel',
        dial_code: '+972',
        code: 'IL'
    }, {
        country: 'Italy',
        dial_code: '+39',
        code: 'IT'
    }, {
        country: 'Jamaica',
        dial_code: '+1876',
        code: 'JM'
    }, {
        country: 'Japan',
        dial_code: '+81',
        code: 'JP'
    }, {
        country: 'Jordan',
        dial_code: '+962',
        code: 'JO'
    }, {
        country: 'Kazakhstan',
        dial_code: '+77',
        code: 'KZ'
    }, {
        country: 'Kenya',
        dial_code: '+254',
        code: 'KE'
    }, {
        country: 'Kiribati',
        dial_code: '+686',
        code: 'KI'
    }, {
        country: 'Kuwait',
        dial_code: '+965',
        code: 'KW'
    }, {
        country: 'Kyrgyzstan',
        dial_code: '+996',
        code: 'KG'
    }, {
        country: 'Latvia',
        dial_code: '+371',
        code: 'LV'
    }, {
        country: 'Lebanon',
        dial_code: '+961',
        code: 'LB'
    }, {
        country: 'Lesotho',
        dial_code: '+266',
        code: 'LS'
    }, {
        country: 'Liberia',
        dial_code: '+231',
        code: 'LR'
    }, {
        country: 'Liechtenstein',
        dial_code: '+423',
        code: 'LI'
    }, {
        country: 'Lithuania',
        dial_code: '+370',
        code: 'LT'
    }, {
        country: 'Luxembourg',
        dial_code: '+352',
        code: 'LU'
    }, {
        country: 'Madagascar',
        dial_code: '+261',
        code: 'MG'
    }, {
        country: 'Malawi',
        dial_code: '+265',
        code: 'MW'
    }, {
        country: 'Malaysia',
        dial_code: '+60',
        code: 'MY'
    }, {
        country: 'Maldives',
        dial_code: '+960',
        code: 'MV'
    }, {
        country: 'Mali',
        dial_code: '+223',
        code: 'ML'
    }, {
        country: 'Malta',
        dial_code: '+356',
        code: 'MT'
    }, {
        country: 'Marshall Islands',
        dial_code: '+692',
        code: 'MH'
    }, {
        country: 'Martinique',
        dial_code: '+596',
        code: 'MQ'
    }, {
        country: 'Mauritania',
        dial_code: '+222',
        code: 'MR'
    }, {
        country: 'Mauritius',
        dial_code: '+230',
        code: 'MU'
    }, {
        country: 'Mayotte',
        dial_code: '+262',
        code: 'YT'
    }, {
        country: 'Mexico',
        dial_code: '+52',
        code: 'MX'
    }, {
        country: 'Monaco',
        dial_code: '+377',
        code: 'MC'
    }, {
        country: 'Mongolia',
        dial_code: '+976',
        code: 'MN'
    }, {
        country: 'Montenegro',
        dial_code: '+382',
        code: 'ME'
    }, {
        country: 'Montserrat',
        dial_code: '+1664',
        code: 'MS'
    }, {
        country: 'Morocco',
        dial_code: '+212',
        code: 'MA'
    }, {
        country: 'Myanmar',
        dial_code: '+95',
        code: 'MM'
    }, {
        country: 'Namibia',
        dial_code: '+264',
        code: 'NA'
    }, {
        country: 'Nauru',
        dial_code: '+674',
        code: 'NR'
    }, {
        country: 'Nepal',
        dial_code: '+977',
        code: 'NP'
    }, {
        country: 'Netherlands',
        dial_code: '+31',
        code: 'NL'
    }, {
        country: 'Netherlands Antilles',
        dial_code: '+599',
        code: 'AN'
    }, {
        country: 'New Caledonia',
        dial_code: '+687',
        code: 'NC'
    }, {
        country: 'New Zealand',
        dial_code: '+64',
        code: 'NZ'
    }, {
        country: 'Nicaragua',
        dial_code: '+505',
        code: 'NI'
    }, {
        country: 'Niger',
        dial_code: '+227',
        code: 'NE'
    }, {
        country: 'Nigeria',
        dial_code: '+234',
        code: 'NG'
    }, {
        country: 'Niue',
        dial_code: '+683',
        code: 'NU'
    }, {
        country: 'Norfolk Island',
        dial_code: '+672',
        code: 'NF'
    }, {
        country: 'Northern Mariana Islands',
        dial_code: '+1670',
        code: 'MP'
    }, {
        country: 'Norway',
        dial_code: '+47',
        code: 'NO'
    }, {
        country: 'Oman',
        dial_code: '+968',
        code: 'OM'
    }, {
        country: 'Pakistan',
        dial_code: '+92',
        code: 'PK'
    }, {
        country: 'Palau',
        dial_code: '+680',
        code: 'PW'
    }, {
        country: 'Panama',
        dial_code: '+507',
        code: 'PA'
    }, {
        country: 'Papua New Guinea',
        dial_code: '+675',
        code: 'PG'
    }, {
        country: 'Paraguay',
        dial_code: '+595',
        code: 'PY'
    }, {
        country: 'Peru',
        dial_code: '+51',
        code: 'PE'
    }, {
        country: 'Philippines',
        dial_code: '+63',
        code: 'PH'
    }, {
        country: 'Poland',
        dial_code: '+48',
        code: 'PL'
    }, {
        country: 'Portugal',
        dial_code: '+351',
        code: 'PT'
    }, {
        country: 'Puerto Rico',
        dial_code: '+1939',
        code: 'PR'
    }, {
        country: 'Qatar',
        dial_code: '+974',
        code: 'QA'
    }, {
        country: 'Romania',
        dial_code: '+40',
        code: 'RO'
    }, {
        country: 'Rwanda',
        dial_code: '+250',
        code: 'RW'
    }, {
        country: 'Samoa',
        dial_code: '+685',
        code: 'WS'
    }, {
        country: 'San Marino',
        dial_code: '+378',
        code: 'SM'
    }, {
        country: 'Saudi Arabia',
        dial_code: '+966',
        code: 'SA'
    }, {
        country: 'Senegal',
        dial_code: '+221',
        code: 'SN'
    }, {
        country: 'Serbia',
        dial_code: '+381',
        code: 'RS'
    }, {
        country: 'Seychelles',
        dial_code: '+248',
        code: 'SC'
    }, {
        country: 'Sierra Leone',
        dial_code: '+232',
        code: 'SL'
    }, {
        country: 'Singapore',
        dial_code: '+65',
        code: 'SG'
    }, {
        country: 'Slovakia',
        dial_code: '+421',
        code: 'SK'
    }, {
        country: 'Slovenia',
        dial_code: '+386',
        code: 'SI'
    }, {
        country: 'Solomon Islands',
        dial_code: '+677',
        code: 'SB'
    }, {
        country: 'South Africa',
        dial_code: '+27',
        code: 'ZA'
    }, {
        country: 'South Georgia and the South Sandwich Islands',
        dial_code: '+500',
        code: 'GS'
    }, {
        country: 'Spain',
        dial_code: '+34',
        code: 'ES'
    }, {
        country: 'Sri Lanka',
        dial_code: '+94',
        code: 'LK'
    }, {
        country: 'Sudan',
        dial_code: '+249',
        code: 'SD'
    }, {
        country: 'Suriname',
        dial_code: '+597',
        code: 'SR'
    }, {
        country: 'Swaziland',
        dial_code: '+268',
        code: 'SZ'
    }, {
        country: 'Sweden',
        dial_code: '+46',
        code: 'SE'
    }, {
        country: 'Switzerland',
        dial_code: '+41',
        code: 'CH'
    }, {
        country: 'Tajikistan',
        dial_code: '+992',
        code: 'TJ'
    }, {
        country: 'Thailand',
        dial_code: '+66',
        code: 'TH'
    }, {
        country: 'Togo',
        dial_code: '+228',
        code: 'TG'
    }, {
        country: 'Tokelau',
        dial_code: '+690',
        code: 'TK'
    }, {
        country: 'Tonga',
        dial_code: '+676',
        code: 'TO'
    }, {
        country: 'Trinidad and Tobago',
        dial_code: '+1868',
        code: 'TT'
    }, {
        country: 'Tunisia',
        dial_code: '+216',
        code: 'TN'
    }, {
        country: 'Turkey',
        dial_code: '+90',
        code: 'TR'
    }, {
        country: 'Turkmenistan',
        dial_code: '+993',
        code: 'TM'
    }, {
        country: 'Turks and Caicos Islands',
        dial_code: '+1649',
        code: 'TC'
    }, {
        country: 'Tuvalu',
        dial_code: '+688',
        code: 'TV'
    }, {
        country: 'Uganda',
        dial_code: '+256',
        code: 'UG'
    }, {
        country: 'Ukraine',
        dial_code: '+380',
        code: 'UA'
    }, {
        country: 'United Arab Emirates',
        dial_code: '+971',
        code: 'AE'
    }, {
        country: 'United Kingdom',
        dial_code: '+44',
        code: 'GB'
    }, {
        country: 'United States',
        dial_code: '+1',
        code: 'US'
    }, {
        country: 'Uruguay',
        dial_code: '+598',
        code: 'UY'
    }, {
        country: 'Uzbekistan',
        dial_code: '+998',
        code: 'UZ'
    }, {
        country: 'Vanuatu',
        dial_code: '+678',
        code: 'VU'
    }, {
        country: 'Wallis and Futuna',
        dial_code: '+681',
        code: 'WF'
    }, {
        country: 'Yemen',
        dial_code: '+967',
        code: 'YE'
    }, {
        country: 'Zambia',
        dial_code: '+260',
        code: 'ZM'
    }, {
        country: 'Zimbabwe',
        dial_code: '+263',
        code: 'ZW'
    }, {
        country: 'Bolivia, Plurinational State of',
        dial_code: '+591',
        code: 'BO'
    }, {
        country: 'Brunei Darussalam',
        dial_code: '+673',
        code: 'BN'
    }, {
        country: 'Cocos (Keeling) Islands',
        dial_code: '+61',
        code: 'CC'
    }, {
        country: 'Congo, The Democratic Republic of the',
        dial_code: '+243',
        code: 'CD'
    }, {
        country: 'Cote d\'Ivoire',
        dial_code: '+225',
        code: 'CI'
    }, {
        country: 'Falkland Islands (Malvinas)',
        dial_code: '+500',
        code: 'FK'
    }, {
        country: 'Guernsey',
        dial_code: '+44',
        code: 'GG'
    }, {
        country: 'Holy See (Vatican City State)',
        dial_code: '+379',
        code: 'VA'
    }, {
        country: 'Hong Kong',
        dial_code: '+852',
        code: 'HK'
    }, {
        country: 'Iran, Islamic Republic of',
        dial_code: '+98',
        code: 'IR'
    }, {
        country: 'Isle of Man',
        dial_code: '+44',
        code: 'IM'
    }, {
        country: 'Jersey',
        dial_code: '+44',
        code: 'JE'
    }, {
        country: 'Korea, Democratic People\'s Republic of',
        dial_code: '+850',
        code: 'KP'
    }, {
        country: 'Korea, Republic of',
        dial_code: '+82',
        code: 'KR'
    }, {
        country: 'Lao People\'s Democratic Republic',
        dial_code: '+856',
        code: 'LA'
    }, {
        country: 'Libyan Arab Jamahiriya',
        dial_code: '+218',
        code: 'LY'
    }, {
        country: 'Macao',
        dial_code: '+853',
        code: 'MO'
    }, {
        country: 'Macedonia, The Former Yugoslav Republic of',
        dial_code: '+389',
        code: 'MK'
    }, {
        country: 'Micronesia, Federated States of',
        dial_code: '+691',
        code: 'FM'
    }, {
        country: 'Moldova, Republic of',
        dial_code: '+373',
        code: 'MD'
    }, {
        country: 'Mozambique',
        dial_code: '+258',
        code: 'MZ'
    }, {
        country: 'Palestinian Territory, Occupied',
        dial_code: '+970',
        code: 'PS'
    }, {
        country: 'Pitcairn',
        dial_code: '+872',
        code: 'PN'
    }, {
        country: 'Réunion',
        dial_code: '+262',
        code: 'RE'
    }, {
        country: 'Russia',
        dial_code: '+7',
        code: 'RU'
    }, {
        country: 'Saint Barthélemy',
        dial_code: '+590',
        code: 'BL'
    }, {
        country: 'Saint Helena, Ascension and Tristan Da Cunha',
        dial_code: '+290',
        code: 'SH'
    }, {
        country: 'Saint Kitts and Nevis',
        dial_code: '+1869',
        code: 'KN'
    }, {
        country: 'Saint Lucia',
        dial_code: '+1758',
        code: 'LC'
    }, {
        country: 'Saint Martin',
        dial_code: '+590',
        code: 'MF'
    }, {
        country: 'Saint Pierre and Miquelon',
        dial_code: '+508',
        code: 'PM'
    }, {
        country: 'Saint Vincent and the Grenadines',
        dial_code: '+1784',
        code: 'VC'
    }, {
        country: 'Sao Tome and Principe',
        dial_code: '+239',
        code: 'ST'
    }, {
        country: 'Somalia',
        dial_code: '+252',
        code: 'SO'
    }, {
        country: 'Svalbard and Jan Mayen',
        dial_code: '+47',
        code: 'SJ'
    }, {
        country: 'Syrian Arab Republic',
        dial_code: '+963',
        code: 'SY'
    }, {
        country: 'Taiwan, Province of China',
        dial_code: '+886',
        code: 'TW'
    }, {
        country: 'Tanzania, United Republic of',
        dial_code: '+255',
        code: 'TZ'
    }, {
        country: 'Timor-Leste',
        dial_code: '+670',
        code: 'TL'
    }, {
        country: 'Venezuela, Bolivarian Republic of',
        dial_code: '+58',
        code: 'VE'
    }, {
        country: 'Viet Nam',
        dial_code: '+84',
        code: 'VN'
    }, {
        country: 'Virgin Islands, British',
        dial_code: '+1284',
        code: 'VG'
    }, {
        country: 'Virgin Islands, U.S.',
        dial_code: '+1340',
        code: 'VI'
    }];

    var cityCodesNL = [{
        dial_code: '+3110',
        city: 'Rotterdam'
    }, {
        dial_code: '+31111',
        city: 'Zierikzee'
    }, {
        dial_code: '+31113',
        city: 'Goes'
    }, {
        dial_code: '+31114',
        city: 'Hulst'
    }, {
        dial_code: '+31115',
        city: 'Terneuzen'
    }, {
        dial_code: '+31117',
        city: 'Oostburg'
    }, {
        dial_code: '+31118',
        city: 'Middelburg'
    }, {
        dial_code: '+3113',
        city: 'Tilburg'
    }, {
        dial_code: '+3114',
        city: 'Municipality'
    }, {
        dial_code: '+3115',
        city: 'Delft'
    }, {
        dial_code: '+31161',
        city: 'Rijen'
    }, {
        dial_code: '+31162',
        city: 'Oosterhout'
    }, {
        dial_code: '+31164',
        city: 'Bergen op Zoom'
    }, {
        dial_code: '+31165',
        city: 'Roosendaal'
    }, {
        dial_code: '+31166',
        city: 'Tholen'
    }, {
        dial_code: '+31167',
        city: 'Steenbergen'
    }, {
        dial_code: '+31168',
        city: 'Zevenbergen'
    }, {
        dial_code: '+31172',
        city: 'Alphen aan den Rijn'
    }, {
        dial_code: '+31174',
        city: 'Naaldwijk'
    }, {
        dial_code: '+31180',
        city: 'Krimpen aan den IJsel'
    }, {
        dial_code: '+31181',
        city: 'Spijkenisse'
    }, {
        dial_code: '+31182',
        city: 'Gouda'
    }, {
        dial_code: '+31183',
        city: 'Gorinchem'
    }, {
        dial_code: '+31184',
        city: 'Sliedrecht'
    }, {
        dial_code: '+31186',
        city: 'Oud-Beijerland'
    }, {
        dial_code: '+31187',
        city: 'Middelharnis'
    }, {
        dial_code: '+3120',
        city: 'Amsterdam'
    }, {
        dial_code: '+31222',
        city: 'Den Burg'
    }, {
        dial_code: '+31223',
        city: 'Den Helder'
    }, {
        dial_code: '+31224',
        city: 'Schagen'
    }, {
        dial_code: '+31226',
        city: 'Noord-Scharwoude'
    }, {
        dial_code: '+31227',
        city: 'Middenmeer'
    }, {
        dial_code: '+31228',
        city: 'Enkhuizen'
    }, {
        dial_code: '+31229',
        city: 'Hoorn'
    }, {
        dial_code: '+3123',
        city: 'Haarlem'
    }, {
        dial_code: '+3124',
        city: 'Nijmegen'
    }, {
        dial_code: '+31251',
        city: 'Beverwijk'
    }, {
        dial_code: '+31252',
        city: 'Hillegom'
    }, {
        dial_code: '+31255',
        city: 'IJmuiden'
    }, {
        dial_code: '+3126',
        city: 'Arnhem'
    }, {
        dial_code: '+31294',
        city: 'Weesp'
    }, {
        dial_code: '+31297',
        city: 'Uithoorn'
    }, {
        dial_code: '+31299',
        city: 'Purmerend'
    }, {
        dial_code: '+3130',
        city: 'Utrecht'
    }, {
        dial_code: '+31313',
        city: 'Dieren'
    }, {
        dial_code: '+31314',
        city: 'Doetinchem'
    }, {
        dial_code: '+31315',
        city: 'Terborg'
    }, {
        dial_code: '+31316',
        city: 'Zevenaar'
    }, {
        dial_code: '+31317',
        city: 'Wageningen'
    }, {
        dial_code: '+31318',
        city: 'Ede'
    }, {
        dial_code: '+31320',
        city: 'Lelystad'
    }, {
        dial_code: '+31321',
        city: 'Dronten'
    }, {
        dial_code: '+3133',
        city: 'Amersfoort'
    }, {
        dial_code: '+31341',
        city: 'Harderwijk'
    }, {
        dial_code: '+31342',
        city: 'Barneveld'
    }, {
        dial_code: '+31343',
        city: 'Doorn'
    }, {
        dial_code: '+31344',
        city: 'Tiel'
    }, {
        dial_code: '+31294',
        city: 'Weesp'
    }, {
        dial_code: '+31297',
        city: 'Uithoorn'
    }, {
        dial_code: '+31299',
        city: 'Purmerend'
    }, {
        dial_code: '+3130',
        city: 'Utrecht'
    }, {
        dial_code: '+31313',
        city: 'Dieren'
    }, {
        dial_code: '+31314',
        city: 'Doetinchem'
    }, {
        dial_code: '+31315',
        city: 'Terborg'
    }, {
        dial_code: '+31316',
        city: 'Zevenaar'
    }, {
        dial_code: '+31317',
        city: 'Wageningen'
    }, {
        dial_code: '+31318',
        city: 'Ede'
    }, {
        dial_code: '+31320',
        city: 'Lelystad'
    }, {
        dial_code: '+31321',
        city: 'Dronten'
    }, {
        dial_code: '+3133',
        city: 'Amersfoort'
    }, {
        dial_code: '+31341',
        city: 'Harderwijk'
    }, {
        dial_code: '+31342',
        city: 'Barneveld'
    }, {
        dial_code: '+31343',
        city: 'Doorn'
    }, {
        dial_code: '+31344',
        city: 'Tiel'
    }, {
        dial_code: '+31345',
        city: 'Culemborg'
    }, {
        dial_code: '+31346',
        city: 'Maarssen'
    }, {
        dial_code: '+31347',
        city: 'Vianen'
    }, {
        dial_code: '+31348',
        city: 'Woerden'
    }, {
        dial_code: '+3135',
        city: 'Hilversum'
    }, {
        dial_code: '+3136',
        city: 'Almere'
    }, {
        dial_code: '+3138',
        city: 'Zwolle'
    }, {
        dial_code: '+3140',
        city: 'Eindhoven'
    }, {
        dial_code: '+31411',
        city: 'Boxtel'
    }, {
        dial_code: '+31412',
        city: 'Oss'
    }, {
        dial_code: '+31413',
        city: 'Veghel'
    }, {
        dial_code: '+31416',
        city: 'Waalwijk'
    }, {
        dial_code: '+31418',
        city: 'Zaltbommel'
    }, {
        dial_code: '+3143',
        city: 'Maastricht'
    }, {
        dial_code: '+3145',
        city: 'Heerlen'
    }, {
        dial_code: '+3146',
        city: 'Sittard'
    }, {
        dial_code: '+31475',
        city: 'Roermond'
    }, {
        dial_code: '+31478',
        city: 'Venray'
    }, {
        dial_code: '+31481',
        city: 'Bemmel'
    }, {
        dial_code: '+31485',
        city: 'Cuijk'
    }, {
        dial_code: '+31486',
        city: 'Grave'
    }, {
        dial_code: '+31487',
        city: 'Druten'
    }, {
        dial_code: '+31488',
        city: 'Zetten'
    }, {
        dial_code: '+31492',
        city: 'Helmond'
    }, {
        dial_code: '+31493',
        city: 'Deurne'
    }, {
        dial_code: '+31495',
        city: 'Weert'
    }, {
        dial_code: '+31497',
        city: 'Eersel'
    }, {
        dial_code: '+31499',
        city: 'Best'
    }, {
        dial_code: '+3150',
        city: 'Groningen'
    }, {
        dial_code: '+31511',
        city: 'Feanwâlden'
    }, {
        dial_code: '+31512',
        city: 'Drachten'
    }, {
        dial_code: '+31513',
        city: 'Heerenveen'
    }, {
        dial_code: '+31514',
        city: 'Balk'
    }, {
        dial_code: '+31515',
        city: 'Sneek'
    }, {
        dial_code: '+31516',
        city: 'Oosterwolde'
    }, {
        dial_code: '+31517',
        city: 'Franeker'
    }, {
        dial_code: '+31518',
        city: 'St Annaparochie'
    }, {
        dial_code: '+31519',
        city: 'Dokkum'
    }, {
        dial_code: '+31521',
        city: 'Steenwijk'
    }, {
        dial_code: '+31522',
        city: 'Meppel'
    }, {
        dial_code: '+31523',
        city: 'Dedemsvaart'
    }, {
        dial_code: '+31524',
        city: 'Coevorden'
    }, {
        dial_code: '+31525',
        city: 'Elburg'
    }, {
        dial_code: '+31527',
        city: 'Emmeloord'
    }, {
        dial_code: '+31528',
        city: 'Hoogeveen'
    }, {
        dial_code: '+31529',
        city: 'Ommen'
    }, {
        dial_code: '+3153',
        city: 'Enschede'
    }, {
        dial_code: '+31541',
        city: 'Oldenzaal'
    }, {
        dial_code: '+31543',
        city: 'Winterswijk'
    }, {
        dial_code: '+31544',
        city: 'Groenlo'
    }, {
        dial_code: '+31545',
        city: 'Neede'
    }, {
        dial_code: '+31546',
        city: 'Almelo'
    }, {
        dial_code: '+31547',
        city: 'Goor'
    }, {
        dial_code: '+31548',
        city: 'Rijssen'
    }, {
        dial_code: '+3155',
        city: 'Apeldoorn'
    }, {
        dial_code: '+31561',
        city: 'Wolvega'
    }, {
        dial_code: '+31562',
        city: 'West-Terschelling'
    }, {
        dial_code: '+31566',
        city: 'Jirnsum'
    }, {
        dial_code: '+31570',
        city: 'Deventer'
    }, {
        dial_code: '+31571',
        city: 'Twello'
    }, {
        dial_code: '+31572',
        city: 'Raalte'
    }, {
        dial_code: '+31573',
        city: 'Lochem'
    }, {
        dial_code: '+31575',
        city: 'Zutphen'
    }, {
        dial_code: '+31577',
        city: 'Uddel'
    }, {
        dial_code: '+31578',
        city: 'Epe'
    }, {
        dial_code: '+3158',
        city: 'Leeuwarden'
    }, {
        dial_code: '+31591',
        city: 'Emmen'
    }, {
        dial_code: '+31592',
        city: 'Assen'
    }, {
        dial_code: '+31593',
        city: 'Beilen'
    }, {
        dial_code: '+31594',
        city: 'Zuidhorn'
    }, {
        dial_code: '+31595',
        city: 'Warffum'
    }, {
        dial_code: '+31596',
        city: 'Appingedam'
    }, {
        dial_code: '+31597',
        city: 'Winschoten'
    }, {
        dial_code: '+31598',
        city: 'Hoogezand'
    }, {
        dial_code: '+31599',
        city: 'Stadskanaal'
    }, {
        dial_code: '+3170',
        city: 'Den Haag'
    }, {
        dial_code: '+3171',
        city: 'Leiden'
    }, {
        dial_code: '+3172',
        city: 'Alkmaar'
    }, {
        dial_code: '+3173',
        city: '\'s-Hertogenbosch'
    }, {
        dial_code: '+3174',
        city: 'Hengelo'
    }, {
        dial_code: '+3175',
        city: 'Zaandam'
    }, {
        dial_code: '+3176',
        city: 'Breda'
    }, {
        dial_code: '+3177',
        city: 'Venlo'
    }, {
        dial_code: '+3178',
        city: 'Dordrecht'
    }, {
        dial_code: '+3179',
        city: 'Zoetermeer'
    }, {
        dial_code: '+3161',
        city: 'Mobile number (NL)'
    }, {
        dial_code: '+3162',
        city: 'Mobile number (NL)'
    }, {
        dial_code: '+3163',
        city: 'Mobile number (NL)'
    }, {
        dial_code: '+3164',
        city: 'Mobile number (NL)'
    }, {
        dial_code: '+3165',
        city: 'Mobile number (NL)'
    }, {
        dial_code: '+3168',
        city: 'Mobile number (NL)'
    }, {
        dial_code: '+3169',
        city: 'Mobile number (NL)'
    }, {
        dial_code: '+3166',
        city: 'Semafonie'
    }, {
        dial_code: '+31670',
        city: 'Videotex'
    }, {
        dial_code: '+31671',
        city: 'Videotex'
    }, {
        dial_code: '+31672',
        city: 'Videotex'
    }, {
        dial_code: '+31673',
        city: 'Videotex'
    }, {
        dial_code: '+31674',
        city: 'Videotex'
    }, {
        dial_code: '+31675',
        city: 'Videotex'
    }, {
        dial_code: '+31676',
        city: 'Dial-in number'
    }, {
        dial_code: '+31800',
        city: 'Free information number'
    }, {
        dial_code: '+3182',
        city: 'Virtual Private Network'
    }, {
        dial_code: '+3188',
        city: 'Company number'
    }, {
        dial_code: '+31900',
        city: 'Paid information number'
    }, {
        dial_code: '+31906',
        city: 'Paid information number'
    }, {
        dial_code: '+31909',
        city: 'Paid information number'
    }, {
        dial_code: '+3191',
        city: 'Place independent number'
    }];

    // These numbers will always be valid.
    var SPECIAL_NUMBERS = ['112', '911'];

    var index = {
        defaultCountry: null,
        /**
         * Format a number by the following rules:
         * 1. Remove all characters except digits, '#', '+' and '*'.
         * 2. If the number starts with a single zero, it is a national number.
         *    Replace the first digit with the country code of the user.
         * 3. If the number starts with 14, prepend country code (14 + municipality area code).
         * 4. If the number starts with two zero's, replace the first two digits with a '+'.
         *
         * @return {String} Formatted international phonenumber.
         */
        formatNumber: function formatNumber(number, countryCode) {
            countryCode = countryCode || this.defaultCountry;

            var normalized = String(number).replace(/[^#*0-9\+]/g, '');
            var country = this.getCountryFromCode(countryCode);

            // Convert national number to international.
            if (/^0[^0]/.test(normalized)) {
                normalized = country.dial_code + normalized.substr(1);
            }

            // Convert beginning '00' to '+'.
            if (/^00/.test(normalized)) {
                normalized = '+' + normalized.substr(2);
            }

            if (/^14/.test(normalized)) {
                normalized = country.dial_code + normalized;
            }

            return normalized;
        },

        /**
         * Detect the country of a formatted phonenumber.
         *
         * @return {Object} Object of country if a corresponding country if found,
         *         Otherwise null.
         */
        getCountryFromNumber: function getCountryFromNumber(number) {
            var isInternal = number.charAt(0) === '*';

            if (isInternal) {
                return this.getInternal();
            }

            if (number.length > 1) {
                return countryCodes.find(function (country) {
                    return number.indexOf(country.dial_code) > -1 && country;
                });
            }

            return null;
        },

        /**
         * Detect the city of a formatted phonenumber.
         *
         * @return {Object} Object of city if a corresponding city if found,
         *         Otherwise null.
         */
        getCityFromNumber: function getCityFromNumber(number) {
            // We only have data from Dutch cities at the moment.
            var chosenCity = null;

            if (number.length > 3) {
                chosenCity = cityCodesNL.find(function (city) {
                    return number.indexOf(city.dial_code) > -1 && city;
                });
            }

            return chosenCity;
        },

        /**
         * Formats the number and looks up the country and city of it.
         *
         * @return {Object} Always returns 'number' (formatted),
         *         and 'country' (long name) and/or 'city' (long name) if found.
         */
        getFormatted: function getFormatted(number, countryCode) {
            var output = {};
            var formattedNumber = this.formatNumber(number, countryCode);
            var country = this.getCountryFromNumber(formattedNumber);
            var city = this.getCityFromNumber(formattedNumber);

            output.number = formattedNumber;

            if (country) {
                output.country = country.country;
            }

            if (country && city) {
                output.city = city.city;
            }

            return output;
        },

        /**
         * Looks up full country info (long name, dial code and short code)
         * of a given short country code (ex: 'nl').
         *
         * @return {Object} Object of country if a corresponding country if found,
         *         Otherwise null.
         */
        getCountryFromCode: function getCountryFromCode(code) {
            code = code.toUpperCase();
            return countryCodes.find(function (country) {
                return code === country.code && country;
            });
        },

        /**
         * Get format of internal number.
         *
         * @return {Object}
         */
        getInternal: function getInternal() {
            return {
                country: 'Internal number',
                dial_code: '*',
                code: 'internal'
            };
        },

        /**
         * @return {boolean} Whether |value| is a (dialable) phone number.
         */
        isValidNumber: function isValidNumber(number) {
            number = String(number);

            // TODO(robwu): Make this pattern a bit stricter.
            if (/^\s*\+?[#*0-9\-\+ ]+$/.test(number)) {
                if (SPECIAL_NUMBERS.indexOf(number) === -1 && number.replace(/\D/g, '').length < 3) {
                    // Must have at least four numbers and not be an alarm number
                    return false;
                }
                return true;
            }
            return false;
        }
    };

    return index;

}));