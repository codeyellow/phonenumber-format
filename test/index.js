import test from 'ava';
import phone from '../dist/phonenumber-format';

test.beforeEach(() => {
    phone.defaultCountry = null;
});

test('phone numbers should be parsed correctly', t => {
    const numbers = {
        '06-12345678': '+31612345678',
        '(06) 12345678': '+31612345678',
        '040 - 2557490': '+31402557490',
        '040 780 50 90': '+31407805090',
        '(00) 31612345678': '+31612345678',
        '(+31) 6 12345678': '+31612345678',
        '(+32) 6 12345678': '+32612345678',
        '*1000': '*1000',
        '*1000 ': '*1000',
        '*007': '*007',
        14040: '+3114040',
        112: '112',
        911: '911',
    };

    for (const [actual, expected] of Object.entries(numbers)) {
        t.is(phone.formatNumber(actual, 'nl'), expected);
    }
});

test('global country code should work', t => {
    phone.defaultCountry = 'nl';
    t.is(phone.formatNumber('06 - 12345678'), '+31612345678');
});

test('phone numbers should be valid', t => {
    const validNumbers = [
        '112',
        '911',
        '14040',
        '*9000',
        '06 - 1235678',
        '+32612345678',
    ];
    const invalidNumbers = ['1', '22', 'aaaaa', '1aaaa2'];

    validNumbers.forEach((number) => {
        t.true(phone.isValidNumber(number));
    });
    invalidNumbers.forEach((number) => {
        t.false(phone.isValidNumber(number));
    });
});

test('getFormatted should return extra phone data', t => {
    const numbers = {
        '(06) 12345678': {
            number: '+31612345678',
            country: 'Netherlands',
            city: 'Mobile number (NL)',
        },
        '0031 - 407805090': {
            number: '+31407805090',
            country: 'Netherlands',
            city: 'Eindhoven',
        },
        '+32612345678': {
            number: '+32612345678',
            country: 'Belgium',
        },
        '*9000': {
            number: '*9000',
            country: 'Internal number',
        },
    };

    for (const [actual, expected] of Object.entries(numbers)) {
        const format = phone.getFormatted(actual, 'nl');
        t.deepEqual(format, expected);
    }
});

test('getCountryFromNumber should return a country', t => {
    const info = phone.getCountryFromNumber('+32022254300');
    t.deepEqual(info, {
        country: 'Belgium',
        dial_code: '+32',
        code: 'BE',
    });
});
