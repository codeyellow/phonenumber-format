# phonenumber-format

Format a phonenumber to an international phonenumber, based on the given country code. Optionally outputs extra data about a phonenumber, such as the country name and city name (only for Dutch cities). Also has support for SIP extensions (like `*9000`).

E.g.:

- `(06)-12345678` would become: `+31612345678`.

## Usage

__formatNumber(number, countryCode)__  
Formats a number. If it's a national number, the `countryCode` will be used to convert it to an international number.

E.g.: `phonenumberFormat.formatNumber('06-12345678', 'nl')`

__isValidNumber(number)__  
Basic function that checks if the given number could be valid. Returns `true` or `false`.

__getFormatted(number, countryCode)__  
This formats the number with `formatNumber` and outputs the country of the number along with it. If it's a Dutch number, the city will also be specified.

Example:  
`phonenumberFormat.getFormatted('040 780 50 90', 'nl')`  
Will output the following object:
```js
{
    number: '+31407805090',
    country: 'Netherlands',
    city: 'Eindhoven'
}
```

__getCountryFromNumber(number)__  
Returns country info of the given  _formatted_ number.

Example:  
`phonenumberFormat.getCountryFromNumber('+32022254300')`  
Will output the following object:  
```js
{
    country: 'Belgium',
    dial_code: '+32',
    code: 'BE'
}
```

## Development

Use `npm t` to run the tests.
